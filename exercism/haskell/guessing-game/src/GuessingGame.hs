module GuessingGame (reply) where

reply :: Int -> String
reply guess
  | guess == answer + 1 || guess == answer - 1 = "So close"
  | guess < answer - 1 = "Too low"
  | guess > answer + 1 = "Too high"
  | otherwise = "Correct"

answer :: Int
answer = 42
