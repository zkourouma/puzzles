import HelloWorld (hello)
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.Runner (configFastFail, defaultConfig, hspecWith)

main :: IO ()
main = hspecWith defaultConfig {configFastFail = True} specs

specs :: Spec
specs =
  describe "hello-world" $
    it "hello" $
      hello `shouldBe` "Hello, World!"
