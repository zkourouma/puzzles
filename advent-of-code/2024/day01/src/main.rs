use std::{collections::HashMap, iter::zip, path::Path};
use utils::read_inputs;

fn main() {
    let input_distance = find_input_distance();
    let similarity = find_input_similarity();
    println!("part 1: {input_distance}, part 2: {similarity}");
}

fn find_input_distance() -> usize {
    let (mut locations_l, mut locations_r) = parse_inputs();
    locations_r.sort();
    locations_l.sort();

    zip(locations_l, locations_r).fold(0, |acc, (left_loc, right_loc)| {
        acc + left_loc.abs_diff(right_loc)
    })
}

fn find_input_similarity() -> usize {
    let (locations_l, locations_r) = parse_inputs();
    let mut frequencies = HashMap::new();

    for right_loc in locations_r {
        frequencies
            .entry(right_loc)
            .and_modify(|right_count| *right_count += 1)
            .or_insert(1);
    }

    locations_l.iter().fold(0, |acc, left_loc| {
        acc + (left_loc * frequencies.get(left_loc).unwrap_or(&0))
    })
}

fn parse_inputs() -> (Vec<usize>, Vec<usize>) {
    let mut locations_l = Vec::new();
    let mut locations_r = Vec::new();

    for line in read_inputs(Path::new("day01.txt")).map_while(Result::ok) {
        let columns: Vec<usize> = line.split_whitespace().flat_map(|v| v.parse()).collect();
        locations_l.push(columns[0]);
        locations_r.push(columns[1]);
    }

    (locations_l, locations_r)
}
