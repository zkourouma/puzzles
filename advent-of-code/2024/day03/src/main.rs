use std::path::Path;

use once_cell::sync::Lazy;
use regex::Regex;
use utils::read_inputs;

static ALL_MUL: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"mul\(([0-9]{1,3}),([0-9]{1,3})\)").expect("unable to construct regex")
});

fn main() {
    let input = parse_inputs();
    let all_total = search_and_capture(ALL_MUL.clone(), &input);
    let maybe_total = maybe_search_and_capture(ALL_MUL.clone(), &input);

    dbg!(all_total, maybe_total);
}

fn search_and_capture(re: Regex, input: &str) -> usize {
    re.captures_iter(input)
        .map(|c| c.extract())
        .flat_map(|(_command, [a, b])| {
            if let (Ok(x), Ok(y)) = (a.parse::<usize>(), b.parse::<usize>()) {
                Some(x * y)
            } else {
                None
            }
        })
        .sum::<usize>()
}

fn maybe_search_and_capture(re: Regex, input: &str) -> usize {
    let mut total = 0;
    for com in input.split("do()") {
        if let Some((do_mul, _dont_mul)) = com.split_once("don't()") {
            total += search_and_capture(re.clone(), do_mul);
        } else {
            total += search_and_capture(re.clone(), com);
        }
    }
    total
}

fn parse_inputs() -> String {
    read_inputs(Path::new("day03.txt"))
        .map_while(Result::ok)
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::{maybe_search_and_capture, parse_inputs, search_and_capture, ALL_MUL};

    #[test]
    fn part_one() {
        let input = parse_inputs();
        assert_eq!(search_and_capture(ALL_MUL.clone(), &input), 174_103_751);
    }

    #[test]
    fn part_two() {
        let input = parse_inputs();
        assert_eq!(
            maybe_search_and_capture(ALL_MUL.clone(), &input),
            100_411_201
        );
    }
}
