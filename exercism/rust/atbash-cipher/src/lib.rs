use std::str;

/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    translate(plain)
        .enumerate()
        .fold(Vec::new(), |mut acc, (idx, c)| {
            // add a space every five chars
            if idx > 0 && idx.rem_euclid(5) == 0 {
                acc.push(' ');
            }
            acc.push(c);
            acc
        })
        .iter()
        .collect()
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    translate(cipher).collect()
}

fn translate(txt: &str) -> impl Iterator<Item = char> + '_ {
    txt.as_bytes().iter().filter_map(translate_)
}

fn translate_(c: &u8) -> Option<char> {
    let mut maybe_char = None;

    // Lowercase char
    if &97 <= c && c <= &122 {
        maybe_char = Some(219 - c);
    // Uppercase char
    } else if &65 <= c && c <= &90 {
        maybe_char = Some(155 - c + 32);
    // Digit
    } else if &48 <= c && c <= &57 {
        maybe_char = Some(*c);
    }

    maybe_char.map(|new_c| new_c as char)
}
