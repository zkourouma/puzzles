/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let code = code.replace(" ", "");

    if code.chars().any(|c| !c.is_numeric()) {
        return false;
    }

    let mut code = code
        .chars()
        .filter_map(|c| c.to_digit(10))
        .collect::<Vec<u32>>();

    if code.len() <= 1 {
        return false;
    }
    validate_codes(&mut code)
}

fn validate_codes(codes: &mut Vec<u32>) -> bool {
    codes.reverse();
    let p = codes
        .iter()
        .enumerate()
        .map(|(i, n)| {
            if i & 1 != 0 {
                let mut m = n * 2;
                if m >= 9 {
                    m -= 9;
                }
                m
            } else {
                *n
            }
        })
        .sum::<u32>();
    p % 10 == 0
}
