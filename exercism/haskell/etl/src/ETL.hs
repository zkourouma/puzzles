module ETL (transform) where

import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T

transform :: Map Int Text -> Map Char Int
transform legacyData = M.fromList $ concat $ map transform' $ M.toList legacyData

transform' :: (Int, Text) -> [(Char, Int)]
transform' (points, letters) = [(c, points) | c <- T.unpack $ T.toLower letters]
