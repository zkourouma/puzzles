module ValentinesDay where

-- Define the function and required algebraic data types (ADT) below.

data Approval
  = Yes
  | No
  | Maybe

data Cuisine
  = Korean
  | Turkish

data Genre
  = Crime
  | Horror
  | Romance
  | Thriller

data Activity
  = BoardGame
  | Chill
  | Movie Genre
  | Restaurant Cuisine
  | Walk Int

rateActivity :: Activity -> Approval
rateActivity (Movie Romance) = Yes
rateActivity (Restaurant Korean) = Yes
rateActivity (Restaurant Turkish) = Maybe
rateActivity (Walk n) = if n < 3 then Yes else if n <= 5 then Maybe else No
rateActivity _ = No
