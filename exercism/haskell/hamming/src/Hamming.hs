{-# LANGUAGE ParallelListComp #-}

module Hamming (distance) where

distance :: String -> String -> Maybe Int
distance xs ys
  | length xs /= length ys = Nothing
  | otherwise = Just dist
  where
    dist = sum [if x == y then 0 else 1 | x <- xs | y <- ys]
