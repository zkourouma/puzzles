module School
  ( School,
    add,
    empty,
    grade,
    sorted,
  )
where

import Data.List (sort)
import Data.Map.Strict as Map

type School = Map.Map Int [String]

add :: Int -> String -> School -> School
add gradeNum student = insertWith (\s ss -> sort $ s ++ ss) gradeNum [student]

grade :: Int -> School -> [String]
grade = findWithDefault []

sorted :: School -> [(Int, [String])]
sorted = toAscList . Map.map sort
