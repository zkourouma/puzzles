use std::{ops::RangeInclusive, path::Path};

use utils::read_inputs;

static ASCENDING_WINDOW: RangeInclusive<i32> = -3..=-1;
static DESCENDING_WINDOW: RangeInclusive<i32> = 1..=3;

/// input data will be lines of space separated numbers
/// each line is a report and each number in the line is a level
/// a report is safe if:
/// -- all levels are increasing or all levels are decreasing
/// -- AND any two adjacent levels are off by 3 >= diff >= 1
fn main() {
    let reports = parse_inputs();
    let zero_tolerance_reports = safety_check(reports.clone(), 0);
    let single_tolerance_reports = safety_check(reports.clone(), 1);

    dbg!(zero_tolerance_reports, single_tolerance_reports);
}

fn safety_check(reports: Vec<Vec<i32>>, tolerance: usize) -> i32 {
    reports
        .into_iter()
        .filter(|report| is_report_safe(tolerance, report.clone()))
        .collect::<Vec<_>>()
        .len() as i32
}

fn is_report_safe(tolerance: usize, report: Vec<i32>) -> bool {
    if report.len() < 2 {
        return true;
    }

    if !check_report(&report) {
        if tolerance > 0 {
            (0..report.len()).any(|idx| {
                let mut try_report = report.clone();
                try_report.remove(idx);
                is_report_safe(tolerance - 1, try_report)
            })
        } else {
            false
        }
    } else {
        true
    }
}

fn check_report(report: &[i32]) -> bool {
    let is_ascending = report[0] < report[1];
    (0..report.len() - 1).all(|idx| {
        let diff = report[idx] - report[idx + 1];
        if is_ascending {
            ASCENDING_WINDOW.contains(&diff)
        } else {
            DESCENDING_WINDOW.contains(&diff)
        }
    })
}

fn parse_inputs() -> Vec<Vec<i32>> {
    let mut reports = Vec::new();

    for line in read_inputs(Path::new("day02.txt")).map_while(Result::ok) {
        let report: Vec<i32> = line.split_whitespace().flat_map(|v| v.parse()).collect();
        reports.push(report);
    }

    reports
}

#[cfg(test)]
mod tests {
    use crate::{parse_inputs, safety_check};

    #[test]
    fn part_one() {
        let reports = parse_inputs();
        assert_eq!(safety_check(reports, 0), 334);
    }

    #[test]
    fn part_two() {
        let reports = parse_inputs();
        assert_eq!(safety_check(reports.clone(), 1), 400);
    }
}
