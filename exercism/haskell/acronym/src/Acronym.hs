module Acronym
  ( abbreviate,
  )
where

import Data.Char
  ( isAlpha,
    isLower,
    isUpper,
    toUpper,
  )

abbreviate :: String -> String
abbreviate xs = map (toUpper . snd) $ filter (filterLeaders indexed) indexed
  where
    indexed = zip [0 ..] xs

filterLeaders :: [(Int, Char)] -> (Int, Char) -> Bool
filterLeaders indexed (idx, c)
  | isUpper c = isCamelCase indexed idx || isStartOfWord indexed idx
  | isAlpha c = isStartOfWord indexed idx
  | otherwise = False

isStartOfWord :: [(Int, Char)] -> Int -> Bool
isStartOfWord _ 0 = True
isStartOfWord indexed idx = elem prev [Just ' ', Just '-', Just '_']
  where
    prev = lookup (idx - 1) indexed

isCamelCase :: [(Int, Char)] -> Int -> Bool
isCamelCase _ 0 = False
isCamelCase indexed idx =
  let curr = lookup idx indexed
      prev = lookup (idx - 1) indexed
   in maybe False isUpper curr && maybe False isLower prev
