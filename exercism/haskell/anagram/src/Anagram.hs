module Anagram (anagramsFor) where

import Data.Char (toLower)
import Data.List (sort)

anagramsFor :: String -> [String] -> [String]
anagramsFor xs = filter (isAnagram xs)

isAnagram :: String -> String -> Bool
isAnagram source candidate | map toLower source == map toLower candidate = False
isAnagram source candidate = sortToLower source == sortToLower candidate

sortToLower :: String -> String
sortToLower s = sort [toLower c | c <- s]
