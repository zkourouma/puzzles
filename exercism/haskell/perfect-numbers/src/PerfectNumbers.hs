module PerfectNumbers (classify, Classification (..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

classify :: Int -> Maybe Classification
classify n
  | n < 1 = Nothing
  | n == aliquot = Just Perfect
  | n < aliquot = Just Abundant
  | n > aliquot = Just Deficient
  | otherwise = Nothing
  where
    aliquot = sum $ factors n

factors :: Int -> [Int]
factors number = [n | n <- [1 .. div number 2], mod number n == 0]
