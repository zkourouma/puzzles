use std::fmt::{Display, Formatter, Result};

#[derive(PartialEq, Debug)]
pub struct Clock {
    hours: i32,
    minutes: i32,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let offset_hours = hours.rem_euclid(24) + (minutes / 60);
        let mut hours = offset_hours.rem_euclid(24);

        if (minutes % 60) < 0 {
            hours -= 1;
        }

        Self {
            hours: hours.rem_euclid(24),
            minutes: minutes.rem_euclid(60),
        }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Self::new(self.hours, self.minutes + minutes)
    }
}

impl Display for Clock {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{:0>2}:{:0>2}", self.hours, self.minutes)
    }
}
