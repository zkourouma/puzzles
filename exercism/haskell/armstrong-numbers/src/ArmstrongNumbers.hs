module ArmstrongNumbers
  ( armstrong,
  )
where

base :: Integral a => a
base = 10

armstrong :: Integral a => a -> Bool
armstrong x = x == (sum $ map (^ (length ds)) ds) where ds = digits x []

digits :: Integral a => a -> [a] -> [a]
digits 0 ds = ds
digits x ds = digits r (q : ds) where (r, q) = x `quotRem` base
