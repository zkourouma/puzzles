use std::{
    fs::File,
    io::{BufRead, BufReader, Lines},
    path::{Path, PathBuf},
};
use once_cell::sync::OnceCell;
use project_root::get_project_root;

static INPUTS_DIR: OnceCell<PathBuf> = OnceCell::new(); 

fn inputs_dir() -> &'static Path {
    INPUTS_DIR.get_or_init(||{ 
        get_project_root().expect("Unable to find project root").join("inputs")
    })
}


pub fn read_inputs(day: &Path) -> Lines<BufReader<File>> {
    let joined_path = inputs_dir().join(day);
    let f = File::open(joined_path).expect("Unable to open file");
    BufReader::new(f).lines()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn finds_the_input_dir() {
        read_inputs(Path::new("day01.txt"));
    }
}
